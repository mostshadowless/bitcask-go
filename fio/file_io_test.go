package fio

import (
	"github.com/stretchr/testify/assert"
	"os"
	"path/filepath"
	"testing"
)

// 测试后移除测试文件，避免对后续的测试造成影响
func destroyFile(name string) {
	if err := os.RemoveAll(name); err != nil {
		panic(err)
	}
}

func TestFileIO_Close(t *testing.T) {
	prefix := "./tmp"
	os.MkdirAll(prefix, 0644)
	path := filepath.ToSlash(prefix + "/" + "a.data")

	fio, err := NewFileIOManager(path)
	defer destroyFile(path)

	assert.Nil(t, err)
	assert.NotNil(t, fio)

	err = fio.Close()
	assert.Nil(t, err)
}

func TestFileIO_Read(t *testing.T) {
	prefix := "./tmp"
	os.MkdirAll(prefix, 0644)
	path := filepath.ToSlash(prefix + "/" + "a.data")

	// 打开文件
	fio, err := NewFileIOManager(path)
	// 销毁文件
	defer destroyFile(path)

	// 验证文件是否打开
	assert.Nil(t, err)
	assert.NotNil(t, fio)

	// 写入数据
	_, err = fio.Write([]byte("key-a"))
	assert.Nil(t, err)

	_, err = fio.Write([]byte("key-b"))
	assert.Nil(t, err)

	// 读数据
	b1 := make([]byte, 5)
	// 读了几个字节
	n, err := fio.Read(b1, 0)
	assert.Equal(t, 5, n)
	// 读出来的数据是否一致
	assert.Equal(t, []byte("key-a"), b1)

	b2 := make([]byte, 5)
	// 读了几个字节
	n, err = fio.Read(b2, 5)
	assert.Equal(t, 5, n)
	// 读出来的数据是否一致
	assert.Equal(t, []byte("key-b"), b2)

	// 需要先关闭fio, 不然利用destroyFile删除文件时会报错
	err = fio.Close()
	if err != nil {
		panic(err)
	}
}

func TestFileIO_Sync(t *testing.T) {
	prefix := "./tmp"
	os.MkdirAll(prefix, 0644)
	path := filepath.ToSlash(prefix + "/" + "a.data")

	// 创建IO管理实例
	fio, err := NewFileIOManager(path)
	// 测试完成后移除文件
	defer destroyFile(path)

	// 测试文件指针是否非空 以及 错误是否为空
	assert.Nil(t, err)
	assert.NotNil(t, fio)

	err = fio.Sync()
	assert.Nil(t, err)

	// 需要先关闭fio, 不然利用destroyFile删除文件时会报错
	err = fio.Close()
	if err != nil {
		panic(err)
	}
}

func TestFileIO_Write(t *testing.T) {
	prefix := "./tmp"
	os.MkdirAll(prefix, 0644)
	path := filepath.ToSlash(prefix + "/" + "a.data")

	// 创建IO管理实例
	fio, err := NewFileIOManager(path)
	// 测试完成后移除文件
	defer destroyFile(path)

	// 测试文件指针是否非空 以及 错误是否为空
	assert.Nil(t, err)
	assert.NotNil(t, fio)

	n, err := fio.Write([]byte(""))
	assert.Equal(t, 0, n)
	assert.Nil(t, err)

	n, err = fio.Write([]byte("bitcask kv"))
	assert.Equal(t, 10, n)
	assert.Nil(t, err)

	n, err = fio.Write([]byte("storage"))
	assert.Equal(t, 7, n)
	assert.Nil(t, err)

	// 需要先关闭fio, 不然利用destroyFile删除文件时会报错
	err = fio.Close()
	if err != nil {
		panic(err)
	}
}

func TestNewFileIOManager(t *testing.T) {
	// 数据文件路径
	// linux 写法
	//path := filepath.Join("/tmp", "a.data")
	// windows 写法
	prefix := "./tmp"
	os.MkdirAll(prefix, 0644)
	path := filepath.ToSlash(prefix + "/" + "a.data")

	// 创建IO管理实例
	fio, err := NewFileIOManager(path)
	// 测试完成后移除文件
	defer destroyFile(path)

	// 测试文件指针是否非空 以及 错误是否为空
	assert.Nil(t, err)
	assert.NotNil(t, fio)

	// 需要先关闭fio, 不然利用destroyFile删除文件时会报错
	err = fio.Close()
	if err != nil {
		panic(err)
	}
}
