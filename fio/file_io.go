package fio

import "os"

// FileIO 标准系统文件IO
type FileIO struct {
	// 系统文件描述符
	fd *os.File
}

// NewFileIOManager 初始化标准文件IO
func NewFileIOManager(fileName string) (*FileIO, error) {
	fd, err := os.OpenFile(
		fileName,
		os.O_CREATE|os.O_RDWR|os.O_APPEND, // 不存在则创建，赋予读写权限，append方式
		DataFilePerm,                      // 文件权限
	)
	if err != nil {
		return nil, err
	}
	return &FileIO{fd: fd}, err
}

// 实现io_manager.go 定义的io操作接口

// Read 从文件的给定位置读取对应的数据
func (fio *FileIO) Read(b []byte, offset int64) (int, error) {
	return fio.fd.ReadAt(b, offset)
}

// Write 写入字节数组到文件中
func (fio *FileIO) Write(b []byte) (int, error) {
	return fio.fd.Write(b)
}

// Sync 持久化数据(将缓冲区的内容写入磁盘)
func (fio *FileIO) Sync() error {
	return fio.fd.Sync()
}

// Close 关闭文件
func (fio *FileIO) Close() error {
	return fio.fd.Close()
}

// 计算文件的大小
func (fio *FileIO) Size() (int64, error) {
	// 获取文件的状态
	stat, err := fio.fd.Stat()
	if err != nil {
		return 0, err
	}
	return stat.Size(), nil
}
