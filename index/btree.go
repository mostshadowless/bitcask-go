package index

import (
	"bitcask-go/data"
	"bytes"
	"github.com/google/btree"
	"sort"
	"sync"
)

// BTree 索引
type BTree struct {
	// btree索引
	tree *btree.BTree

	// 读写锁(某些操作需要加锁)
	lock *sync.RWMutex
}

// NewBTree 新建BTree索引结构
func NewBTree() *BTree {
	return &BTree{
		tree: btree.New(32), // 节点的度数
		lock: new(sync.RWMutex),
	}
}

// 在这里实现 index.go 定义的三个接口 Put,Get,Delete

// Put 向索引中存储 key 对应的数据位置信息
// 需要加锁
func (bt *BTree) Put(key []byte, pos *data.LogRecordPos) bool {
	// 先构造Item
	it := &Item{key: key, pos: pos}
	// 获取锁
	bt.lock.Lock()
	// 往btree中插入数据
	bt.tree.ReplaceOrInsert(it)
	// 释放锁
	bt.lock.Unlock()

	return true
}

// Get 根据 key 取出对应的索引位置信息
func (bt *BTree) Get(key []byte) *data.LogRecordPos {
	// 构造Item
	it := &Item{key: key}
	// 根据key在btree中查找
	btreeItem := bt.tree.Get(it)
	if btreeItem == nil {
		return nil
	}

	return btreeItem.(*Item).pos
}

// Delete 根据 key 删除对应的索引位置信息
func (bt *BTree) Delete(key []byte) bool {
	// 构造Item
	it := &Item{key: key}
	// 加锁
	bt.lock.Lock()
	// Delete()会返回被删除的btree节点
	oldItem := bt.tree.Delete(it)
	// 释放锁
	bt.lock.Unlock()
	if oldItem == nil {
		return false
	}

	return true
}

func (bt *BTree) Size() int {
	return bt.tree.Len()
}

// BTree 迭代器的实现
type btreeIterator struct {
	currIndex int     // 当前遍历的下标位置
	reverse   bool    // 是否反向遍历
	values    []*Item // key+位置索引信息
}

func (bt *BTree) Iterator(reverse bool) Iterator {
	if bt.tree == nil {
		return nil
	}

	// 读锁
	bt.lock.RLock()
	defer bt.lock.RUnlock()
	return newBTreeIterator(bt.tree, reverse)
}

// 创建btree迭代器实例
func newBTreeIterator(tree *btree.BTree, reverse bool) *btreeIterator {
	var idx int
	values := make([]*Item, tree.Len())

	// 将所有的数据存放到数组中（这里相当于把内存中的所有Key又拷贝了一份, 因此内存占用会增大）
	// todo 内存占用优化
	savaValues := func(it btree.Item) bool {
		values[idx] = it.(*Item)
		idx++
		return true
	}
	if reverse {
		tree.Descend(savaValues)
	} else {
		tree.Ascend(savaValues)
	}

	return &btreeIterator{
		currIndex: 0,
		reverse:   reverse,
		values:    values,
	}
}

func (bti *btreeIterator) Rewind() {
	bti.currIndex = 0
}

func (bti *btreeIterator) Seek(key []byte) {
	if bti.reverse {
		bti.currIndex = sort.Search(len(bti.values), func(i int) bool {
			return bytes.Compare(bti.values[i].key, key) <= 0
		})
	} else {
		bti.currIndex = sort.Search(len(bti.values), func(i int) bool {
			return bytes.Compare(bti.values[i].key, key) >= 0
		})
	}
}

func (bti *btreeIterator) Next() {
	bti.currIndex += 1
}

func (bti *btreeIterator) Valid() bool {
	return bti.currIndex < len(bti.values)
}

func (bti *btreeIterator) Key() []byte {
	return bti.values[bti.currIndex].key
}

func (bti *btreeIterator) Value() *data.LogRecordPos {
	return bti.values[bti.currIndex].pos
}

func (bti *btreeIterator) Close() {
	bti.values = nil
}
