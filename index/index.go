package index

import (
	"bitcask-go/data"
	"bytes"
	"github.com/google/btree"
)

// Indexer 抽象索引接口，后续支持更换其他类型的内存数据结构(如跳表/红黑树)
type Indexer interface {
	// Put 向索引中存储 key 对应的数据位置信息
	Put(key []byte, pos *data.LogRecordPos) bool

	// Get 根据 key 取出对应的索引位置信息
	Get(key []byte) *data.LogRecordPos

	// Delete 根据 key 删除对应的索引位置信息
	Delete(key []byte) bool

	// Size 获取索引中key的数量
	Size() int

	// Iterator 索引迭代器
	Iterator(reverse bool) Iterator
}

type IndexType = int8

const (
	// BTree 索引
	Btree IndexType = iota + 1
	// ART 自适应基数索引
	ART
)

// NewIndexer 根据类型初始化索引
func NewIndexer(typ IndexType) Indexer {
	switch typ {
	case Btree:
		return NewBTree()
	case ART:
		// todo
		return nil
	default:
		panic("unsupported index type")
	}
}

// Item 在这里定义 BTree 中叶子的数据项
type Item struct {
	key []byte
	pos *data.LogRecordPos
}

// Less 用于比较 Item之间的大小
// google/btree 中定义了一个Item接口，并且里面有Less方法
// 我们利用自己定义的Item结构体实现Less方法
// 等同于我们的 Item 实现了 btree里面的接口Item
// bi 一个接口类型变量
// bi.(*Item) 转换接口类型变量
func (ai *Item) Less(bi btree.Item) bool {
	return bytes.Compare(ai.key, bi.(*Item).key) == -1
}

type Iterator interface {
	// Rwind 重新回到迭代器的起点
	Rewind()

	// Seek 根据传入的key查找到第一个大于（或小于）等于的目标key，从这个key开始遍历
	Seek(key []byte)

	// Next 跳转到下一个key
	Next()

	// Valid 当前迭代器的对象是否有效，即hasNext
	Valid() bool

	// Key 当前遍历位置的key数据
	Key() []byte

	// Value 当前遍历位置的value数据
	Value() *data.LogRecordPos

	// Close 关闭迭代器, 释放相应资源
	Close()
}
