package bitcask_go

import (
	"bitcask-go/data"
	"bitcask-go/index"
	"errors"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
)

// DB bitcask 存储引擎实例
type DB struct {
	options    Options                   // 开放给用户的参数
	mutex      *sync.RWMutex             // 锁
	fileIds    []int                     // 文件id，只能在加载索引的时候使用，不能在其他的地方更新和使用
	activaFile *data.DataFile            // 当前活跃数据文件，可以用于写入
	olderFiles map[uint32]*data.DataFile // 旧的数据文件，只能用于读
	index      index.Indexer             // 内存索引
}

// Open 打开 bitcask存储引擎实例
func Open(options Options) (*DB, error) {
	// 对用户传入的配置项进行校验
	if err := checkOptions(options); err != nil {
		return nil, err
	}

	// 判断数据目录是否存在，如果不存在的话，则创建这个目录
	if _, err := os.Stat(options.DirPath); os.IsNotExist(err) {
		if err := os.MkdirAll(options.DirPath, os.ModePerm); err != nil {
			return nil, err
		}
	}

	// 初始化 DB 实例结构体
	db := &DB{
		options:    options,
		mutex:      new(sync.RWMutex),
		olderFiles: make(map[uint32]*data.DataFile),
		index:      index.NewIndexer(options.IndexType),
	}

	// 加载数据文件
	if err := db.loadDataFiles(); err != nil {
		return nil, err
	}

	// 从数据文件中加载索引
	if err := db.loadIndexFromDataFiles(); err != nil {
		return nil, err
	}

	return db, nil
}

// Close 关闭数据库
func (db *DB) Close() error {
	if db.activaFile == nil {
		return nil
	}
	db.mutex.Lock()
	defer db.mutex.Unlock()

	//	关闭当前活跃文件
	if err := db.activaFile.Close(); err != nil {
		return err
	}
	// 关闭旧的数据文件
	for _, file := range db.olderFiles {
		if err := file.Close(); err != nil {
			return err
		}
	}
	return nil
}

// Sync 持久化数据文件
func (db *DB) Sync() error {
	if db.activaFile == nil {
		return nil
	}
	db.mutex.Lock()
	defer db.mutex.Unlock()
	return db.activaFile.Sync()
}

// Put 写入 Key/Value 数据,Key不能为空
func (db *DB) Put(key []byte, value []byte) error {
	// 判断key是否有效
	if len(key) == 0 {
		return ErrKeyIsEmpty
	}

	// 构造 LogRecord 结构体
	logRecord := &data.LogRecord{
		Key:   key,
		Value: value,
		Type:  data.LogRecordNormal,
	}

	// 追加写入到当前活跃数据文件当中
	pos, err := db.appendLogRecord(logRecord)
	if err != nil {
		return err
	}

	// 更新内存索引
	if ok := db.index.Put(key, pos); !ok {
		return ErrIndexUpdateFailed
	}

	return nil
}

// Get 根据 Key 读取数据
func (db *DB) Get(key []byte) ([]byte, error) {
	// 在这里加个读锁
	db.mutex.RLock()
	defer db.mutex.RUnlock()

	// 判断 key 的有效性
	if len(key) == 0 {
		return nil, ErrKeyIsEmpty
	}

	// 从内存结构中取出 key 对应的索引信息
	logRecordPos := db.index.Get(key)
	// 如果 key 不存在内存索引中，说明 key 不存在
	if logRecordPos == nil {
		return nil, ErrKeyNotFound
	}

	return db.getValueByPosition(logRecordPos)
}

// Delete 根据Key删除对应的数据
func (db *DB) Delete(key []byte) error {
	// 判断 key 的有效性
	if len(key) == 0 {
		return ErrKeyIsEmpty
	}

	// 先检查 key 是否存在，如果不存在的话直接返回
	if pos := db.index.Get(key); pos == nil {
		return nil
	}

	// 构造 LogRecord，标识其是被删除的
	logRecord := &data.LogRecord{Key: key, Type: data.LogRecordDeleted}
	// 写入到数据文件当中
	_, err := db.appendLogRecord(logRecord)
	if err != nil {
		return nil
	}

	// 从内存索引中将对应的key删除
	ok := db.index.Delete(key)
	if !ok {
		return ErrIndexUpdateFailed
	}
	return nil
}

func (db *DB) getValueByPosition(logRecordPos *data.LogRecordPos) ([]byte, error) {
	// 根据文件id找到对应的数据文件
	// 先跟当前的活跃文件匹配，若不满足则在旧的数据文件中查找
	var dataFile *data.DataFile
	if db.activaFile.FileId == logRecordPos.Fid {
		dataFile = db.activaFile
	} else {
		dataFile = db.olderFiles[logRecordPos.Fid]
	}

	// 数据文件为空
	if dataFile == nil {
		return nil, ErrDataFileNotFound
	}

	// 根据偏移读取对应的数据
	logRecord, _, err := dataFile.ReadLogRecord(logRecordPos.Offset)
	if err != nil {
		return nil, err
	}

	// 判断当前记录的状态是否有效
	if logRecord.Type == data.LogRecordDeleted {
		return nil, ErrKeyNotFound
	}

	return logRecord.Value, err
}

// 追加写数据到活跃文件中
func (db *DB) appendLogRecord(logRecord *data.LogRecord) (*data.LogRecordPos, error) {
	// 此过程需要加锁
	db.mutex.Lock()
	defer db.mutex.Unlock()

	// 判断当前活跃数据文件是否存在，因为数据库在没有写入的时候是没有文件生成的
	// 如果为空则初始化数据文件
	if db.activaFile == nil {
		if err := db.setActiveDataFile(); err != nil {
			return nil, err
		}
	}

	// 写入数据编码
	encRecord, size := data.EncodeLogRecord(logRecord)

	// 如果写入的数据已经到达了活跃文件的阈值，则关闭活跃文件，并创建新的文件
	if db.activaFile.WriteOff+size > db.options.DataFileSize {
		// 先持久化数据文件，包装已有的数据持久到磁盘中
		if err := db.activaFile.Sync(); err != nil {
			return nil, err
		}

		// 当前活跃文件转为旧的数据文件
		db.olderFiles[db.activaFile.FileId] = db.activaFile

		// 创建新的数据文件作为活跃文件
		if err := db.setActiveDataFile(); err != nil {
			return nil, err
		}
	}

	writeOff := db.activaFile.WriteOff
	if err := db.activaFile.Write(encRecord); err != nil {
		return nil, err
	}

	// 根据用户配置决定是否持久化
	if db.options.SyncWrites {
		if err := db.activaFile.Sync(); err != nil {
			return nil, err
		}
	}

	// 构造内存索引信息
	pos := &data.LogRecordPos{Fid: db.activaFile.FileId, Offset: writeOff}

	return pos, nil
}

// 设置当前活跃文件(此方法每调用一次就能创建一个数据文件)
// 在访问此方法必须持有互斥锁
func (db *DB) setActiveDataFile() error {
	var initialFileId uint32 = 0

	// 存在活跃文件, 则让文件id+1
	if db.activaFile != nil {
		initialFileId = db.activaFile.FileId + 1
	}

	// 若当前没有活跃文件，直接创建一个新文件
	dataFile, err := data.OpenDataFile(db.options.DirPath, initialFileId)
	if err != nil {
		return err
	}

	// 设置当前DB实例的活跃文件
	db.activaFile = dataFile

	return nil
}

// 检测数据库启动的配置项
func checkOptions(options Options) error {
	if options.DirPath == "" {
		return errors.New("database dir path is empty")
	}
	if options.DataFileSize == 0 {
		return errors.New("database data file size must be greater than 0")
	}
	return nil
}

// 从磁盘中加载数据文件
func (db *DB) loadDataFiles() error {
	dirEntries, err := os.ReadDir(db.options.DirPath)
	if err != nil {
		return err
	}

	var fileIds []int
	// 遍历目录中的所有文件，找到所有以".data"结尾的文件
	for _, entry := range dirEntries {
		if strings.HasSuffix(entry.Name(), data.DataFileNameSuffix) {
			splitNames := strings.Split(entry.Name(), ".")
			fileId, err := strconv.Atoi(splitNames[0])
			// 数据目录有可能被损坏了
			if err != nil {
				return ErrDataDirectoryCorrupted
			}
			fileIds = append(fileIds, fileId)
		}
	}

	// 对文件id进行排序，从小到大依次加载
	sort.Ints(fileIds)
	db.fileIds = fileIds

	// 遍历每个文件id，打开对应的数据文件
	for i, fid := range fileIds {
		dataFile, err := data.OpenDataFile(db.options.DirPath, uint32(fid))
		if err != nil {
			return err
		}
		if i == len(fileIds)-1 { // 最后一个fielid指向当前的活跃文件
			db.activaFile = dataFile
		} else {
			db.olderFiles[uint32(fid)] = dataFile
		}
	}
	return nil
}

// 从数据文件中加载索引
// 遍历文件中的所有记录，并更新到内存索引中
func (db *DB) loadIndexFromDataFiles() error {
	// 没有文件，说明数据库是空的，直接返回
	if len(db.fileIds) == 0 {
		return nil
	}

	// 遍历所有的文件id,处理文件中的记录
	for i, fid := range db.fileIds {
		var fileId = uint32(fid)
		var dataFile *data.DataFile

		// 先判断 活跃文件/旧数据文件
		if fileId == db.activaFile.FileId {
			dataFile = db.activaFile
		} else {
			dataFile = db.olderFiles[fileId]
		}

		// 读取当前文件里面的所有记录
		var offset int64 = 0
		for {
			logRecord, size, err := dataFile.ReadLogRecord(offset)
			if err != nil { // 这里不能直接返回, 我们要判断是否读到了文件结尾，这个是正常的状态
				if err == io.EOF {
					break // 当前文件处理完毕
				}
				return err
			}

			// 构造内存索引并保存
			logRecordPos := &data.LogRecordPos{Fid: fileId, Offset: offset}

			// 判断该记录是否被删除
			if logRecord.Type == data.LogRecordDeleted {
				db.index.Delete(logRecord.Key) // 直接从内存索引中删除当前数据
			} else {
				db.index.Put(logRecord.Key, logRecordPos) // 更新索引
			}

			// 递增 offset, 下一次从新的位置开始读取
			offset += size
		}

		// 如果是当前活跃文件，更新这个文件的 WriteOff
		if i == len(db.fileIds)-1 {
			db.activaFile.WriteOff = offset
		}
	}
	return nil
}
