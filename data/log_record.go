package data

import (
	"encoding/binary"
	"hash/crc32"
)

type LogRecordType = byte

// iota为特殊常量
// iota在const关键字出现时被重置为（const内部的第一行之前）,const内每增一行，iota增加1
// iota可以被用作枚举值
const (
	LogRecordNormal LogRecordType = iota
	LogRecordDeleted
)

// header的组成, 定义header的最大长度（其中keySize, valueSize为变长）
// 在这里利用 binary.MaxVarintLen32 对变长的keySize/valueSize的最大长度做一个限制
// crc   type   keySize   valueSize
//	4     1       5           5

const maxLogRecordHeaderSize = binary.MaxVarintLen32*2 + 5

// LogRecord 写入到数据文件的记录
type LogRecord struct {
	Key   []byte
	Value []byte
	Type  LogRecordType // 标记当前记录是否有效
}

// LogRecordPos 数据内存索引，快速检索数据在磁盘上的位置
type LogRecordPos struct {
	// 文件id,表示将数据存储到了哪个文件
	Fid uint32

	// 当前文件内的数据偏移量
	Offset int64
}

// LogRecord 的头部信息
type LogRecordHeader struct {
	crc        uint32        // crc 校验值
	recordType LogRecordType // 标识LogRecord的类型
	keySize    uint32        // key的长度
	valueSize  uint32        // value的长度
}

// EncodeLogRecord 对 LogRecord 进行编码，返回字节数组及长度
//
//	+-------------+-------------+-------------+--------------+-------------+--------------+
//	| crc 校验值  |  type 类型   |    key size |   value size |      key    |      value   |
//	+-------------+-------------+-------------+--------------+-------------+--------------+
//	    4字节          1字节        变长（最大5）   变长（最大5）     变长           变长
func EncodeLogRecord(logRecord *LogRecord) ([]byte, int64) {
	// 初始化一个 haeder 部分的字节数组
	header := make([]byte, maxLogRecordHeaderSize)

	// header[:4] 存储 crc校验值，先把这块空出来

	// 数据的类型
	header[4] = logRecord.Type
	var index = 5

	// header[5:] 存储 keySize 和 valueSize, 这两个是变长的
	// 将 int64 编码到 buf中，并返回写入的字节数
	index += binary.PutVarint(header[index:], int64(len(logRecord.Key)))
	index += binary.PutVarint(header[index:], int64(len(logRecord.Value)))

	// size 存储整个logRecord 的长度
	var size = index + len(logRecord.Key) + len(logRecord.Value)

	// 存储编码后的logRecord
	encBytes := make([]byte, size)

	// 将header部分的内容存储起来
	copy(encBytes[:index], header[:index])
	// 将 key 和 value 数据拷贝到字节数组中
	copy(encBytes[index:], logRecord.Key)
	copy(encBytes[index+len(logRecord.Key):], logRecord.Value)

	// 对整个LogRecord的数据进行crc校验
	crc := crc32.ChecksumIEEE(encBytes[4:])
	binary.LittleEndian.PutUint32(encBytes[:4], crc)

	return encBytes, int64(size)
}

// DecodeLogRecordHeader 对字节数组中的 Header信息进行编码
func DecodeLogRecordHeader(buf []byte) (*LogRecordHeader, int64) {
	if len(buf) <= 4 {
		return nil, 0
	}

	header := &LogRecordHeader{
		crc:        binary.LittleEndian.Uint32(buf[:4]),
		recordType: buf[4],
	}

	var index = 5
	// 取出实际的 key size
	keySize, n := binary.Varint(buf[index:])
	header.keySize = uint32(keySize)
	index += n

	// 取出实际的 value size
	valueSize, n := binary.Varint(buf[index:])
	header.valueSize = uint32(valueSize)
	index += n

	return header, int64(index)
}

// GetLogRecordCRC 计算LogRecord的crc值
func GetLogRecordCRC(lr *LogRecord, header []byte) uint32 {
	if lr == nil {
		return 0
	}

	// 此处的header不包含logRecord的crc部分
	crc := crc32.ChecksumIEEE(header[:])
	crc = crc32.Update(crc, crc32.IEEETable, lr.Key)
	crc = crc32.Update(crc, crc32.IEEETable, lr.Value)

	return crc
}
