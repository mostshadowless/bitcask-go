package bitcask_go

import (
	"bitcask-go/index"
	"os"
)

// Options 开放给用户的参数
type Options struct {
	// 数据库数据目录
	DirPath string

	// 数据文件大小
	DataFileSize int64

	// 每次写数据是否持久化
	SyncWrites bool

	// 索引的实现方式
	IndexType index.IndexType
}

// IteratorOptions 索引迭代器配置项
type IteratorOptions struct {
	// 遍历前缀为指定的key, 默认为空
	Prefix []byte

	// 是否反向遍历, 默认false为正向遍历
	Reverse bool
}

var DefaultOptions = Options{
	DirPath:      os.TempDir(),
	DataFileSize: 256 * 1024 * 1024, // 256MB
	SyncWrites:   false,
	IndexType:    index.Btree,
}

var DefaultIteratorOptions = IteratorOptions{
	Prefix:  nil,
	Reverse: false,
}
